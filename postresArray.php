<?php
function postresArray()
{
    $postres = [
        [
            'nombre' => 'Carbayones',
            'comunidad' => 'Principado de Asturias',
            'abreviatura' => 'AST',
            'srcFotos' => [
                'index' => 'imgs/carbayones.jpg',
                'plantilla' => 'imgs/carbayones2.jpg',
            ],
            'ingredientes' => [
                '1 lámina de hojaldre rectangular(250 g.) y mantequilla para untar los moldes de los carbayones.',
                'Para el relleno:',
                '250 g. de almendra molida, 250 g. de azúcar blanquilla,',
                '3 yemas de huevo y un huevo entero (yema y clara), 10 ml. de ron o vino dulce y la ralladura de 1 limón.',
                'Para el baño de yema:',
                '4 huevos L, 200 ml de agua y 200 g de azúcar blanquilla,',
                '20 g de almidón o harina de maíz refinada tipo maicena.',
                'Para decorar, el baño blanco de azúcar o glaseado de claras:',
                'Unas gotas de zumo de limón, 50 g de clara de huevo y 200 g de azúcar glass.',
                'Para esta receta sería importante: Moldes de carbayones(12 unidades) o moldes alargados de magdalenas'
            ],
            'historia' => 'Los carbayones, se remonta a 1924 cuando el Ayuntamiento de Oviedo, pidió a la familia repostera de Blas, en concreto de la mano de su maestro obrador don Jose Gutiérrez, que elaborase un dulce con el que conquistar a los visitantes de la primera edición del Fidma, Feria Internacional de Muestras de Asturias. El nombre de este exquisito pastel se decidió en una reunión en la farmacia Migoya. Uno de los asistentes sugirió carbayón (aumentativo de carbayu, que significa roble en asturiano) para homenajear al emblemático roble de la calle Uría.',
        ],
        [
            'nombre' => 'Casadielles',
            'comunidad' => 'Principado de Asturias',
            'abreviatura' => 'AST',
            'srcFotos' => [
                'index' => 'imgs/casadielles.jpg',
                'plantilla' => 'imgs/casadielles2.jpg',
            ],
            'ingredientes' => [
                '450 g. de harina de trigo',
                '2 cucharaditas tipo postre de levadura química',
                '150 ml. de vino blanco',
                '150 ml. de aceite oliva virgen extra suave',
                '1 yema de huevo grande',
                '1 pizca de sal (3 gramos)',
                'Para el relleno casadielles: 250 g. nueces peladas',
                '150 g. de azúcar',
                '6 cucharadas soperas de leche entera',
                '4 cucharadas soperas de anís',
                'Para decorar: Azúcar para espolvorear',
                'Aceite suave de oliva para freír',
            ],
            'historia' => 'Las casadielles asturianas forman parte de ese grupo de dulces ligados a una festividad concreta. Su consumo está generalizado en algunas zonas de Asturias en Navidad aunque en otras se degusta durante las fiestas de carnaval. Como postre es magnífico tanto por su sencillez como por su sabor.',
        ],
        [
            'nombre' => 'Quesada Pasiega',
            'comunidad' => 'Cantabria',
            'abreviatura' => 'CAN',
            'srcFotos' => [
                'index' => 'imgs/quesada.jpg',
                'plantilla' => 'imgs/quesada2.jpg',
            ],
            'ingredientes' => [
                '250 g de mantequilla',
                '250 g de azúcar',
                '250 g de harina',
                'sal',
                '3 huevos',
                'limón',
                'levadura en polvo',
                'ron',
            ],
            'historia' =>
            'La Quesada pasiega es uno de los postres mas emblemáticos de la gastronomía cántabra cuyo origen esta en los valles pasiegos. 
                Junto con los sobaos, es una de las principales señas de identidad de la repostería regional. El autentico sabor aun lo podemos
                encontrar si visitamos localidades como Selaya, Vega de Pas, San Pedro del Romeral o Alceda',
            'Una de las mayores curiosidades de este postre, es que a pesar de su nombre, la Quesada original, no lleva queso y por eso,
                tiene un sabor muy diferente a otras tartas de queso que elaboran en otras provincias españolas.',

        ],
        [
            'nombre' => 'Goxua',
            'comunidad' => 'País Vasco',
            'abreviatura' => 'PAV',
            'srcFotos' => [
                'index' => 'imgs/goxua.jpg',
                'plantilla' => 'imgs/goxua2.jpg',
            ],
            'ingredientes' => [
                'Para el bizcocho:',
                '4 huevos, 120 g de harina de repostería y 120 g de azúcar.',
                'Para la crema pastelera:',
                '1/2 litro de letre, 150 g de azúcar, 3 huevos,',
                '5 cucharadas de harina refinada y 1 cucharada de extracto de vainilla.',
                'Para la nata montada:',
                '200 g de nata para montar y 35 g de azúcar.',
                'Para el almíbar:',
                '50 g de azúcar, 50 g. de agua, 1 cucharada de ron,',
                '1 pizca de sal, frambuesa y menta para decorar.',
            ],
            'historia' =>
            'Ea goxua, ("dulce, rico" en euskera) es un postre típico del País Vasco, especialmente de la ciudad de Vitoria, 
                proveniente en origen de Miranda de Ebro. 
                Aunque popularmente su invención es atribuida al pastelero vitoriano Luis López de Sosoaga por la década de los 70, 
                el pastelero mirandés Alberto Bornachea afirma que el goxua lo inventó su padre tratando de copiar la crema catalana, 
                bautizando el postre resultante como cazuelita.',

        ],
        [
            'nombre' => 'Yemas de Ávila',
            'comunidad' => 'Castilla y León',
            'abreviatura' => 'CYL',
            'srcFotos' => [
                'index' => 'imgs/yemasdeavila.jpg',
                'plantilla' => 'imgs/yemasdeavila2.jpg',
            ],
            'ingredientes' => [
                '12 yemas de huevo',
                '180 g de azúcar',
                'una ramita de canela en rama',
                '1/2 corteza de limón',
                'azúcar glass',
            ],
            'historia' =>
            'Lo que hoy es Santa Teresa tiene su origen en una pastelería artesana conocida como La Flor de Castilla, cuya historia se remonta a 1860. 
                Fue entonces cuando Isabelo Sánchez ideó la receta de un nuevo dulce, que se sigue elaborando más de 150 años después y es un icono de la ciudad de Ávila. 
                Aquel postre eran las yemas de Santa Teresa, marca de un producto que el propio fundador registró desde el principio, para protegerlo de los muchos imitadores. 
                Su popularidad fue tan grande que los mayores aún recuerdan las tradicionales colas que se formaban en la plaza de José Tomé hasta agotar las existencias.',

        ],
        [
            'nombre' => 'Paparajotes',
            'comunidad' => 'Murcia',
            'abreviatura' => 'MUR',
            'srcFotos' => [
                'index' => 'imgs/paparajotes.jpg',
                'plantilla' => 'imgs/paparajotes2.jpg',
            ],
            'ingredientes' => [
                '600 g de harina de trigo',
                '200ml de agua',
                '200ml de leche',
                'ralladura de 1 limón verde',
                'hojas de limonero(que tengan un tono verde claro, ni muy duras ni muy tiernas)',
                '200 g de azúcar',
                'canela',
                '3 huevos',
                'aceite de oliva',
            ],
            'historia' =>
            'La introducción de los paparajotes en la Región de Murcia se remonta a los sefardíes (judíos de origen árabe) que estaban especializados en los
                conocidos como "postres de sartén". Este tipo de postres estuvieron presentes en casi toda la península a lo largo del siglo XV para, finalmente,
                caer en manos de campesinos y hacendados. La herencia fue tratada con delicadeza y respeto por los huertanos murcianos que lo introdujeron en sus cocinas labriegas.
                Estos trabajadores lo elaboraban diariamante para servirlo después de cada comida al tiempo que lo acompañaban con café de olla (agua de cebada) o café de puchero.',

        ],
        [
            'nombre' => 'Almojábanas ',
            'comunidad' => 'Andalucía',
            'abreviatura' => 'AND',
            'srcFotos' => [
                'index' => 'imgs/almojabanas1.jpg',
                'plantilla' => 'imgs/almojabanas2.jpg',
            ],
            'ingredientes' => [
                '3 huevos',
                '100 g queso fresco de cabra o semicurado',
                '1/4 kg harina (recomiendo 300 g)',
                '1/2 vaso aceite de oliva virgen extra 125 ml.',
                '1/4 l agua 250 ml',
                '1/2 sobre levadura 8 g.',
            ],
            'historia' => 'Las Almojabánas son una especie de torta, herencia de la dulcería árabe en la tradición y en el nombre, pues en el árabe español «al muyabbanat» significa «hecha de queso», como define Covarrubias en Tesoro de la lengua castellana (1611). ',
        ],
        [
            'nombre' => 'Perrunillas extremeñas',
            'comunidad' => 'Extremadura',
            'abreviatura' => 'EXT',
            'srcFotos' => [
                'index' => 'imgs/perrunillas1.jpg',
                'plantilla' => 'imgs/perrunillas2.jpg',
            ],
            'ingredientes' => [
                '125g Manteca de cerdo ibérico a temperatura ambiente',
                '125g Anís dulce (licor)',
                '4 huevos M',
                '150g Azúcar',
                'media ralladura de limón',
                '500g Harina de repostería',
                '15 g de levadura química',
                'Azúcar extra para cubrir',
            ],
            'historia' =>  'Este delicioso dulce tradicional de, probablemente, origen conventual, lo tiene todo para despertar espíritus nostálgicos y recuerdos de infancia. Con su curioso nombre y sus ingredientes humildes, encontramos multitud de versiones de la receta de perrunillas, con variantes regionales pero también en cada familia. Tienen esa magia de los dulces de toda la vida, con recetas de tradición oral que van cambiando con el paso de las generaciones, y que cada familia hace suya.',
        ],
        [
            'nombre' => 'Crema catalana',
            'comunidad' => 'Cataluña',
            'abreviatura' => 'CAT',
            'srcFotos' => [
                'index' => 'imgs/cremacatalana1.jpg',
                'plantilla' => 'imgs/cremacatalana2.jpg',
            ],
            'ingredientes' => [
                '1l de leche entera',
                '8 yemas de huevo',
                '100 g de azúcar',
                '20 g maicena',
                'Corteza de limón al gusto',
                'Corteza de naranja al gusto',
                'Azúcar para caramelizar al gusto',
                'Canela en rama',

            ],
            'historia' => 'La crema catalana o crema quemada es una especie de crema pastelera o natilla con una costra crujiente a base de azúcar caramelizado. Dice la leyenda que su invención fue como otros muchos postres debido a un error, cuando en un convento unas monjas quisieron hacer un flan para un obispo que las iba a visitar y como suele ocurrir en algunas ocasiones, no les quedo bien cuajado.',
        ],
        [
            'nombre' => 'Bienmesabe',
            'comunidad' => 'Canarias',
            'abreviatura' => 'IC',
            'srcFotos' => [
                'index' => 'imgs/bienmesabe1.jpg',
                'plantilla' => 'imgs/bienmesabe2.jpg',
            ],
            'ingredientes' => [
                '250 gramos de almendras molidas (a ser posible de Tejeda o Valsequillo)',
                '4 yemas de huevo',
                '250 gramos de azúcar',
                '250 mililitros de agua',
                'el zumo de un limón',
                'un palo de canela',
            ],
            'historia' => 'Data desde los tiempos de la conquista de Canarias, y es probablemente una adaptación de las tradiciones pasteleras del sur de la península ibérica. Aunque en la actualidad este postre se comercializa por diferentes islas del Archipiélago Canario, como Lanzarote, Tenerife o La Gomera, es en la isla de La Palma donde el bienmesabe tiene una mayor tradición en su elaboración. Es importante nombrar a Matilde Arroyo (1926-2014), vecina y repostera palmera, considerada la “madre del bienmesabe”, labor que le valió el reconocimiento de la Medalla de Oro de Canarias en el año 2009 como pionera en llevar y comercializar la rica tradición palmera de la dulcería más allá de sus fronteras. En Gran Canaria, más concretamente en Tejeda, pueblo de almendros, el bienmesabe cobra también especial protagonismo.',
        ],
        [
            'nombre' => 'Tarta de santiago',
            'comunidad' => 'Galicia',
            'abreviatura' => 'GAL',
            'srcFotos' => [
                'index' => 'imgs/santiago1.jpg',
                'plantilla' => 'imgs/santiago2.jpg',
            ],
            'ingredientes' => [
                '250 g de almendra molida cruda variedad Marcona o 250 g de almendra sin piel (si vais a hacer vosotros la harina)',
                '5 huevos grandes L',
                '250 g de azúcar blanquilla',
                'La ralladura de la piel de 1/2 limón (sólo lo amarillo)',
                '1/2 cuchara pequeña rasa de canela molida (unos 5 g)',
                '1 trocito de mantequilla para engrasar el molde',
                '1/2 chupito de aguardiente de hierbas o del licor que más te guste',
                '50 g de azúcar molido o glass para adornar la tarta',
            ],
            'historia' => 'La primera tarta de Santiago aparece documentada en 1577 durante una visita de D. Pedro de Porto a Compostela. La primera aparición documentada es de Luis Bartolomé de Leybar en el libro Cuaderno de Confitería (1835 Epígrafe de «Tarta de Almendra»)',
        ],
    ];
    return $postres;
};

<?php
require_once "postresArray.php";
$postres = postresArray();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="estilos.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>
    <script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.js"></script>
    <style>
        button#detalles {
            border: none;
            margin: 0;
            padding: 0;
        }
    </style>

</head>

<body>
    <?php
    require_once "_menu.php";
    ?>
    <div class="listaDesplegable">
        <form class="lista" action="plantillaDatos.php">
            <div>
                <select name="postres" id="postres">

                    <option value="">Selecciona postre</option>
                    <?php
                    for ($i = 0; $i < count($postres); $i++) {
                    ?>
                        <option value="<?= $i ?>"> <?= $postres[$i]["nombre"] . " (" . $postres[$i]["comunidad"] . ")" ?></option>

                    <?php
                    }
                    ?>
                </select>
            </div>
            <div>
                <button class="btn btn-info  botonBuscar" name="buscar"><ion-icon name="search-outline"></ion-icon></button>
            </div>
        </form>
    </div>

    <div class="tablaPostres">
        <?php
        for ($i = 0; $i < count($postres); $i++) {
        ?>
            <form action="plantillaDatos.php">
                <div>
                    <!-- Mostramos la comunidad en un párrafo -->
                    <p> <?= $postres[$i]["comunidad"] ?> </p>
                    <!-- Mostramos la foto y es un enlace a plantillaDatos -->
                    <button id='detalles' name='detalles'> <img src='  <?= $postres[$i]['srcFotos']['index'] ?> ' width='250px' height='250px'></button>

                    <!-- Mostramos el nombre de la receta en un párrafo -->
                    <p> <?= $postres[$i]["nombre"] ?> </p>


                    <input type="hidden" name="posicion" value=" <?= $i ?> ">
                    <!-- Botón para ir a los detalles de la receta-->
                    <button class="btn btn-success" name="detalles">Ver información</button>

            </form>
    </div>
<?php
        }
?>


</body>

</html>
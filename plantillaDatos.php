<?php
// cargamos el fichero que contiene el array con los datos
require_once "postresArray.php";
$postres = postresArray();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="estilos.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>
</head>

<body>
    <!-- Insertamos el menu-->
    <?php
    require_once "_menu.php";

    $posicion = null;
    // Si se ha pulsado el botón de detalles de la tablaPostres
    if (isset($_GET["detalles"])) {
        $posicion = intval($_GET["posicion"]);
    }

    // Si se ha pulsado el botón buscar de la lista desplegable
    if (isset($_GET["buscar"])) {
        $posicion = intval($_GET["postres"]);
    }
    ?>


    <!-- Comienzo de la muestra de datos del libro seleccionado-->

    <div class="mostrarPostre">

        <!-- Mostramos la foto en un div-->
        <div class="plantillaFoto">
            <img src=' <?= $postres[$posicion]["srcFotos"]['plantilla'] ?>' width="300px" height="300px" style="border-radius: 30px" alt="">
        </div>

        <!-- Mostramos el nombre y la comunidad en otro div-->
        <div class="plantillaInformacion">
            <h3 class="plantillaCentrar"><?= $postres[$posicion]["nombre"] ?></h3>
            <h3 class="plantillaCentrar"><?= $postres[$posicion]["comunidad"] ?></h3>
            <h3>Ingredientes</h3>
            <ul class="ingredientes">
                <?php
                for ($i = 0; $i < count($postres[$posicion]['ingredientes']); $i++) {
                ?>
                    <li> <?= $postres[$posicion]['ingredientes'][$i] ?></li>
                <?php
                }
                ?>
            </ul>

        </div>

        <!-- Mostramos en un lista los ingredientes de la receta -->
        <div class="plantillaHistoria">
            <h3>Historia</h3>
            <p> <?= $postres[$posicion]['historia'] ?></p>
        </div>

        <!-- Botón para volver al index-->
        <div class="botonPastel">
            <a href="index.php" class="botonVolver"><img class="pastel" src="imgs/cup-cake.png" alt=""></a>
        </div>

    </div>

    <!-- Fin de la muestra de datos del libro seleccionado-->
</body>

</html>